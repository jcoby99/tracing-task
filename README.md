## Module 11: Distributed tracing and monitoring

### Sub-task 1 – CloudWatch

**1. Create CloudWatch Rule to make your lambda run on weekly base**

![screenshot](screenshots/1/1.1.png)

![screenshot](screenshots/1/1.2.png)

![screenshot](screenshots/1/1.3.png)

![screenshot](screenshots/1/1.4.png)

![screenshot](screenshots/1/1.5.png)

**2. Update your lambda by adding some logging, trigger lambda manually and ensure CloudWatch logs were created**

![screenshot](screenshots/1/2.1.png)

![screenshot](screenshots/1/2.2.png)

![screenshot](screenshots/1/2.3.png)

**3. Explore CloudWatch metrics for your EC2 instances by visiting the Monitoring tab**

![screenshot](screenshots/1/3.png)

**4. Create CloudWatch Alarm for the EC2 instance CPUUtilization and ensure that it works**

![screenshot](screenshots/1/4.1.png)

![screenshot](screenshots/1/4.2.png)

![screenshot](screenshots/1/4.3.png)

![screenshot](screenshots/1/4.4.png)

![screenshot](screenshots/1/4.5.png)

### Sub-task 2 - CloudTrail

**Explore Event History section:**

- EC2 instances

![screenshot](screenshots/2/1.png)

- RDS

![screenshot](screenshots/2/2.png)

- SNS

![screenshot](screenshots/2/3.png)

- S3

![screenshot](screenshots/2/4.png)

- DynamoDB

![screenshot](screenshots/2/5.png)
